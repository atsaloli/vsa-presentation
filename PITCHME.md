---?color=linear-gradient(to right, #2193b0, #6dd5ed)
@title[Introduction]

@snap[west text-25 text-bold text-white]
Vertical Sysadmin<br>*The Company*
@snapend

@snap[south-west byline text-white text-06]
Real learning - presented so that you're able to understand.
@snapend

---
@title[About us]

### About us

<br><br>

- Founded: 2009
- Employees: 2
- Annual receipts: 500K+ in 2018
  - 444K consulting
  - 87K training


---
@title[Purpose]

### Purpose

<br><br>

- To maintain the IT fabric of civilization by:
  - effective training on excellent technologies
  - implementation consulting


---
@title[Clients]

### Clients

<br><br>

- AT&T
- University of Edinburgh
- Lawrence Berkeley National Laboratory
- USENIX (professional association)
- Diamond Light Source (UK particle accelerator)
- Deluxe Technicolor Digital Cinema
- Samsung
- LinkedIn Corporation

---
@title[Partners]

### Partners

#### Authorized training partner for:
<br><br>

- Northern.tech (CFEngine)
- Linux Professional Institute (LPI)
- GitLab

---
@title[Technology]

### Technology

#### We can train/consult on:
<br><br>

- Linux essentials and system administration
- Docker / Kubernetes
- AWS / Terraform
- CFEngine / Ansible
- Perl / Python / Bash scripting
- Version control with Git
- DevOps / Agile


---?image=template/img/questions-3.png&size=auto 60%
@title[Q&A]
@snap[east]
@snapend


---?color=#2193b0
<!-- #6dd5ed -->
@title[Get In Touch]

@snap[east]
@css[contact-name](Aleksey Tsalolikhin)<br>
@css[twitter-handle text-white](@atsaloli)
@fa[twitter-square text-white pad-left-icon]<br>
@css[contact-email text-white](aleksey@verticalsysadmin.com)
@fa[envelope-o text-white pad-left-icon]<br>
<br>
@snapend

@snap[west span-50]
<!-- ![CONTACT-2](template/img/contact-2.png) -->
@snapend

@snap[north-west text-white template-note]

@snapend
